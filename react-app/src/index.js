import { BrowserRouter, Routes, Route } from 'react-router-dom';
import React from 'react';
import './index.css';
import reportWebVitals from './reportWebVitals';
import PageNotFound from './Components/PageNotFound/PageNotFound';
import PageIndex from './Components/PageIndex/PageIndex'
import Page from './Components/Page/Page';
import { createRoot } from "react-dom/client";
import { Provider } from "react-redux";
import { store } from "./Components/Store/Store";



const rootElement = document.getElementById("root");
const root = createRoot(rootElement);

root.render(

  <React.StrictMode>
    <BrowserRouter>
      <Provider store={store}>
      <Routes>
        <Route path="/" element={<PageIndex />} />
        <Route path="/Page" element={<Page />} />
        <Route path="*" element={<PageNotFound />} />
      </Routes>
    </Provider>
  </BrowserRouter>

  </React.StrictMode >
);


reportWebVitals();
