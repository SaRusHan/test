import Header from "../Header/Header";
import Footer from "../Footer/Footer";
import { Link } from 'react-router-dom';
import './pageIndex.css';

function PageIndex() {
    return (
        <>
            <div>
                <Header />
            </div>
            <div>
                <p className="text">Здесь должно быть содержимое главной страницы.<br />
                    Но в рамках курса главная страница используется лишь<br />
                    в демонстрационных целях.
                    <Link to="/Page">Перейти на страницу товара</Link>
                </p>
            </div>
            <div className="footer">
                <Footer />
            </div>

        </>

    );
}
export default PageIndex;