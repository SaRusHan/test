import './container.css';
import Colors from '../Colors/Colors';
import Memory from '../Memory/Memory';
import Sidebar from '../Sidebar/Sidebar';
import Link from '../Link/Link';

function Main() {
    return (
        <div className="container">
            <div className="product">
                <div className="choise-color">
                    <div className="choise-color__text">
                        <b>Цвет товара</b>
                    </div>
                    <Colors />
                </div>

                <div className="configuration">
                    <div className="configuration__memory">Конфигурация памяти</div>
                    <Memory />
                </div>
                <div className="characteristic">
                    <div className="characteristic__text">Характеристики товара</div>
                    <div className="characteristic-list">
                        <ul className="characteristic__list">
                            <li className="characteristic__product">Экран: <b>6.1</b></li>
                            <li className="characteristic__product">Встроенная память: <b>128 ГБ</b></li>
                            <li className="characteristic__product">Операционная система: <b>iOS 15</b></li>
                            <li className="characteristic__product">Беспроводные интерфейсы:<b>NFC, Bluetooth, Wi-Fi</b>
                            </li>
                            <li className="characteristic__product">Процессор:<Link
                                link = "https://en.wikipedia.org/wiki/Apple_A15" text ="AppleA15
                                Bionic" /></li>
                            <li className="characteristic__product">Вес: <b>173 г</b></li>
                        </ul>
                    </div>

                </div>
                <div className="opisanie">
                    <div className="opisanie__text">Описание</div>
                    <div className="opisanie__paragraph">
                        <div className="opisanie__paragraph__text">
                            Наша самая совершенная система двух камер.<br />
                            Особый взгляд на прочность дисплея.<br />
                            Чип, с которым всё супербыстро.<br />
                            Аккумулятор держится заметно дольше.<br />
                            <i>iPhone 13 - сильный мира всего.</i>
                        </div>
                        <div className="opisanie__paragraph__text">
                            Мы разработали совершенно новую схему расположения и развернули объективы на45
                            градусов. Благодаря этому внутри корпуса поместилась наша лучшая система
                            двух камер с увеличенной матрицей широкоугольной камеры.
                            Кроме того, мы освободили место для системы оптической стабилизации изображения
                            сдвигом матрицы. И повысили скорость работы матрицы на сверхширокоугольной камере.
                        </div>
                        <div className="opisanie__paragraph__text">
                            Новая сверхширокоугольная камера видит больше деталей в тёмных областях снимков.
                            Новая широкоугольная камера улавливает на 47% больше света для более
                            качественных фотографий и видео. Новая оптическая стабилизация со сдвигом
                            матрицы обеспечит чёткие кадры даже в неустойчивом положении.
                        </div>
                        <div className="opisanie__paragraph__text">
                            Режим «Киноэффект»
                            автоматически добавляет великолепные эффекты перемещения фокуса и изменения
                            резкости. Просто начните запись видео. Режим «Киноэффект» будет удерживать
                            фокус на объекте съёмки, создавая красивый эффект размытия вокруг
                            него. Режим «Киноэффект» распознаёт, когда нужно перевести фокус на другого человека
                            или объект, который появился в кадре. Теперь ваши видео будут смотреться как
                            настоящее кино.
                        </div>
                    </div>
                    <div className="comparison">
                        <div className="comparison-text">Сравнение моделей</div>
                        <table className="table">
                            <tbody className="table__body">
                                <tr className="table__line-head">
                                    <th className="table__head">Модель</th>
                                    <th className="table__head">Вес</th>
                                    <th className="table__head">Высота</th>
                                    <th className="table__head">Ширина</th>
                                    <th className="table__head">Толщина</th>
                                    <th className="table__head">Чип</th>
                                    <th className="table__head">Память</th>
                                    <th className="table__head">Аккумулятор</th>
                                </tr>
                                <tr className="table__line">
                                    <td className="table__cell">iPhone 11</td>
                                    <td className="table__cell">194 грамма </td>
                                    <td className="table__cell">150.9 мм</td>
                                    <td className="table__cell">75.7 мм</td>
                                    <td className="table__cell">8.3 мм</td>
                                    <td className="table__cell">A13 Bionic chip</td>
                                    <td className="table__cell">до 128 Гб</td>
                                    <td className="table__cell">До 17 часов</td>
                                </tr>
                                <tr className="table__line">
                                    <td className="table__cell">iPhone 12</td>
                                    <td className="table__cell">164 грамма </td>
                                    <td className="table__cell">146.7 мм</td>
                                    <td className="table__cell">71.5 мм</td>
                                    <td className="table__cell">7.4 мм</td>
                                    <td className="table__cell">A14 Bionic chip</td>
                                    <td className="table__cell">до 256 Гб</td>
                                    <td className="table__cell">До 19 часов</td>
                                </tr>
                                <tr className="table__line">
                                    <td className="table__cell">iPhone 13</td>
                                    <td className="table__cell">174 грамма </td>
                                    <td className="table__cell">146.7 мм</td>
                                    <td className="table__cell">71.5 мм</td>
                                    <td className="table__cell">7.65 мм</td>
                                    <td className="table__cell">A15 Bionic chip</td>
                                    <td className="table__cell">до 512 Гб</td>
                                    <td className="table__cell">До 19 часов</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <Sidebar />
        </div>
    );
}
export default Main;