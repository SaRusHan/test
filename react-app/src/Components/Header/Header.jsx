import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import './logo.css';
import logotyp from './apple.png';
import heart from './heart1.svg';
import Korzinka from './Korzina.svg';

function Header() {
    const cart = useSelector((store) => store.cart);
    return (
        <div className="logo-back">
            <div className='logo-margin'>
                <div className='logo'>
                    <div className='logo__name'>
                        <Link to="/">
                            <div className="logo__iconka">
                                <img className="img" src={logotyp} alt="Логотип" />
                            </div>
                        </Link>

                        <div className="logo__text">
                            <Link to="/">
                                <span className="logo__text_color">Мой</span><samp className='shop'>Маркет</samp>
                            </Link>
                        </div>
                    </div>
                    <div className='cart__choice'>
                    <img className="favourites" src={heart} alt="сердце" />
                    <div className={`favourites__choice ${cart.favorit ? '' : 'hidden'}`}>1</div>
                    <img className="logo__basket" src={Korzinka} alt="корзина" />
                    <div className={`logo__choice ${cart.products ? '' : 'hidden'}`}>1</div>
                    </div>
               </div>
            </div>
        </div >
    );
}
export default Header;
