import image1 from './image-1.webp';
import image2 from './image-2.webp';
import image3 from './image-3.webp';
import image4 from './image-4.webp';
import image5 from './image-5.webp';
import './product.css';

function Product() {
    return (
        <div>
            <div className="type-product">
                <b>Смартфон Apple iPhone 13, синий</b>
            </div>
            <div className="picture-product">
                <img className="picture-product__img1" src={image1} alt="Айфон вид спереди и сзади" />
                <img className="picture-product__img2" src={image2} alt="Изображение Айфона, лицевая сторона" />
                <img className="picture-product__img3" src={image3} alt="Изображение Айфона по углом" />
                <img className="picture-product__img4" src={image4} alt="Изображение камеры Айфон " />
                <img className="picture-product__img5" src={image5} alt="Айфон вид спереди и сзади" />
            </div>
        </div>
    );
}
export default Product;