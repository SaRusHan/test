import { createSlice } from "@reduxjs/toolkit";

export const cartSlice = createSlice({

    name: "cart",

    initialState: { products: 0, favorit: 0 },


    reducers: {

        addProducts: (prevState, action) => {
            return {
                products: 1 - prevState.products,
                favorit: prevState.favorit
        };
    },
        addFavorit: (prevState, action) => {
            return {
                products: prevState.products,
                favorit: 1 - prevState.favorit
        };
    },
    },
});

export const { addProducts, addFavorit } = cartSlice.actions;

export default cartSlice.reducer;