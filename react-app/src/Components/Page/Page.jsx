import React from 'react';
import Header from "../Header/Header";
import BreadCrumbs from "../Breadcrumbs/Breadcrumbs";
import Footer from "../Footer/Footer";
import Product from "../Product/Product";
import Main from "../Main/Main";
import CommentList from "../Comment/CommentList";
import review from '../Comment/review';
import Forma from "../Forma/Forma";


function Page() {
    return (
        <div className="super-tag">
            <Header />
            <BreadCrumbs />
            <Product />
            <Main />
            <CommentList review={review} />
            <Forma /> 
            <Footer />
        </div>
    );
}
export default Page;