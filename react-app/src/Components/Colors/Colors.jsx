import { useState } from 'react';
import React from 'react';
import './colors.css';

let colorList = [
    {
        id: 1,
        image: "/img/color-1.webp",
        alt: "Айфон красного цвета"
    },
    {
        id: 2,
        image: "/img/color-2.webp",
        alt: "Айфон зеленого цвета"
    },
    {
        id: 3,
        image: "/img/color-3.webp",
        alt: "Айфон розового цвета"
    },
    {
        id: 4,
        image: "/img/color-4.webp",
        alt: "Айфон бирюзового цвета"
    },
    {
        id: 5,
        image: "/img/color-5.webp",
        alt: "Айфон белого цвета"
    },
    {
        id: 6,
        image: "/img/color-6.webp",
        alt: "Айфон черного цвета"
    }
];

function Colors() {
let [activeInput, setActiveInput] = useState(3);
let hendleClick=(_e, id)=> {
    setActiveInput(id);
};
    return (
        <div className="choise-color__bloc">
            {colorList.map((colorList) => {
                let active = colorList.id === activeInput;
                let activeClass = active ? "choise-color__box_selected" : "";
                return (
                    <label className={`choise-color__box ${activeClass}`} key={colorList.id}>
                        <input 
                        className="color-iphone"
                        type="radio"
                        name="color"
                        value=""
                        onClick={(e) => hendleClick(e, colorList.id)} />
                        <img className="choise-color__box__image"
                            src={colorList.image} alt={colorList.alt} />
                    </label>
                );
            })}
        </div>
    );
}
export default Colors;
