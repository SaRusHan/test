
import { useSelector, useDispatch } from "react-redux";
import { addProducts, addFavorit } from '../Cart/cart-reducer';
import Iframe from "../Iframe/Iframe";
import './sidbar.css'


function Sidebar() {

    const cart = useSelector((store) => store.cart);
    const dispatch = useDispatch();

    const hendleClickProducts = () => dispatch(addProducts());
    const hendleClickFavorit = () => dispatch(addFavorit());

  
    return (
        <div className="sidebar">
            <div className="sale">
                <div className="sale__tender">
                    <div className="sale__tender__cent">
                        <div className="sale__tender__cent__old">75 990₽</div>
                        <span className="sale__tender__cent__discount">-8%</span>
                        <div onClick={hendleClickFavorit} className={`sale__tender__favourites ${cart.favorit === 1 ? 'favourites__coise' : ''}`}></div>
                    </div>
                    <div className="sale__tender__new">67 990₽</div>
                </div>
                <div className="sale__delivery">
                    <div className="sale__delivery__pickup">Самовывоз в четверг, 1 сентября — <b>бесплатно</b>
                    </div>
                    <div className="sale__delivery__courier">Курьером в четверг, 1 сентября — <b>бесплатно</b>
                    </div>
                </div>
                <div><button onClick={hendleClickProducts} className={`sale__basket ${cart.products === 1 ? "colors" : ""}`}>{cart.products === 0 ? 'Добавить в корзину' : 'Товар уже в корзине'}</button></div>
            </div>
            <div className="marketing">Реклама
                <div className="marketing__news">
                    <div className="iframe">
                        <Iframe />
                    </div>
                </div>
                <div className="marketing__news">
                    <div className="iframe">
                        <Iframe />
                    </div>
                </div>
            </div>
        </div>
    );
};
export default Sidebar;