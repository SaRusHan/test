import Link from '../Link/Link';
import React from "react";
import styled from "styled-components";


const Container = styled.div`
margin-top: 53px;
margin-left: 51px;
display: flex;
padding: 8px 0;
gap: 4px;
color: #008CF0;
@media (max-width: 1024px) {
margin-left: 15px;
}
@media (max-width: 1024px) {
font-weight: 400;
font-size: 14px;
line-height: 17px;
flex-wrap: wrap;
}
@media (max-width: 360px) {
margin-top: 35px;
}
`;



let navlist = [
    {
        text: "Электроника",
        link: "/PageNotFound.jsx"
    },
    {
        text: "Смартфоны и гаджеты",
        link: "/PageNotFound.jsx"
    },
    {
        text: "Мобильные телефоны",
        link: "/PageNotFound.jsx"
    },
    {
        text: "Apple",
        link: "/PageNotFound.jsx"
    },
];

function BreadCrumbs(props) {

    return (
        <Container>
            {
                navlist.map(({ text, link }, index) => <Link text={text} link={link} index={index} key={text} />)
            }
        </Container>
    );
}

export default BreadCrumbs;
