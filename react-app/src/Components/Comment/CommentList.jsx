import CommentItem from "./CommentItem";
import './comment.css';

function CommentList(props) {
    let { review } = props;


    return (
        <div>
            <div className="comment">
                <div className="comment__blok">
                    <div className="comment__blok__text">Отзывы</div>
                    <div className="comment__blok__number">425</div>
                </div>
            </div>

            {
                review.map((commentItem) => <CommentItem commentItem={commentItem} key={commentItem.id} />)

            }
            
        </div >
    );
}
export default CommentList;