import './comment.css';

function CommentItem(props) {
    let { commentItem } = props;

    return (
        <div>
            <div className="box">
                <div className="box-one">
                    <img className="box-one__img-comit" src={commentItem.img1} alt={commentItem.alt} />
                    <div className="feedback">
                        <div className="feedback__user">{commentItem.name}</div>
                        <div className="feedback__stars">
                            <img src={commentItem.img2} alt={commentItem.alt2} />
                            <img src={commentItem.img3} alt={commentItem.alt3} />
                            <img src={commentItem.img4} alt={commentItem.alt4} />
                            <img src={commentItem.img5} alt={commentItem.alt5} />
                            <img src={commentItem.img6} alt={commentItem.alt6} />
                        </div>
                        <div className="feedback__practice">
                            <div><b>{commentItem.practic}</b></div>
                            <div><b>{commentItem.plus}</b></div>
                            <div>{commentItem.text1}</div>
                            <div><b>{commentItem.minus}</b></div>
                            <div>{commentItem.text2}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default CommentItem;