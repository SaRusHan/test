import { useState } from 'react';
import './memory.css';

let memory = [
    {
        id: 1,
        text: "128ГБ",
    },
    {
        id: 2,
        text: "256ГБ",
    },
    {
        id: 3,
        text: "512ГБ",
    }
];

function Memory() {
    let [activeInput, setActiveInput] = useState(1);
    let hendleClick = (_e, id) => {
        setActiveInput(id);
    };
    return (
        <div className="configuration-button">
            {memory.map((memory) => {
                let active = memory.id === activeInput;
                let activeClass = active ? "configuration-button__text_selected" : "";
                return (
                    <label
                        className={`configuration-button__text ${activeClass}`} key={memory.id}>
                        <input 
                            className="memory-btn"
                            type="radio"
                            name="memory"
                            value=""
                            onClick={(e) => hendleClick(e, memory.id)} />
                        {`${memory.text}`}
                    </label>
                );
            })}
        </div>
    );
}
export default Memory;

