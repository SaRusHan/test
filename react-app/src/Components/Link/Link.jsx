import "./link.css";

function Link(props) {
    let { text, link, index } = props;
    return (
        <>
            <div className="link">
                {index ? <span>{">"}</span> : null}
                <a href={link}>{text}</a>
            </div>
        </>
    );
}
export default Link;