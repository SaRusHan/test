import styles from "./Footer.module.css";
import { useCurrentDate } from "@kundinos/react-hooks";

function Footer() {
    const currentDate = useCurrentDate();

    const fullYear = currentDate.getFullYear();

    return (
        <footer>
            <div className={styles.down}>
                <div className={styles.down__info}>
                    <b>© ООО «<span className={styles.logo__text}>Мой</span>Маркет», 2018-{`${fullYear}`}.</b><br />
                    Для уточнения информации звоните по номеру <a href="tel:+79000000000"> +7 900 000 0000 </a>,<br />
                    а предложения по сотрудничеству отправляйте на почту<a
                        href="mailto:partner@mymarket.com">partner@mymarket.com</a>
                </div>
                <div className={styles.down__back}><a href="#Top">Наверх</a></div>
            </div>
        </footer>
    );
}
export default Footer;