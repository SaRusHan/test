import { useState } from 'react';
import './form.css';

let errorInitial = { name: null, score: null, review: null };

function Forma(props) {

    let [input, setInput] = useState(JSON.parse(localStorage.getItem('reviewFormInput')));
    let [error, setError] = useState({ ...errorInitial });
    localStorage.setItem('reviewFormInput', JSON.stringify(input));

    let handleChangeName = (event) => { setInput({ ...input, name: event.target.value }) };
    let handleFocusName = (event) => { setError({ ...errorInitial }) };
    let handleChangeScore = (event) => { setInput({ ...input, score: (event.target.value) }) };
    let handleFocusScore = (event) => { setError({ ...errorInitial }) };
    let handleChangeReview = (event) => { setInput({ ...input, review: event.target.value }) };
    let handleFocusReview = (event) => { setError({ ...errorInitial }) };

    let handleSubmit = (event) => {
        event.preventDefault();

        let newError = {};

        if (input?.name.length < 2) {
            newError.name = "Имя не может быть короче 2-х символов";
        }
        if (input?.name.trim() === "") {
            newError.name = "Вы забыли указать Имя и Фамилию";
        }
        if (input?.score < 1 || input?.score > 5) {
            newError.score = "Укажите число от 1 до 5";
        }
        if (input?.review === "") {
            newError.review = "Вы забыли оставить отзыв";
        }

        if (newError.name && newError.score) {
            newError.score = "";
        }
        if (newError.name && newError.score && newError.review) {
            newError.score = "";
            newError.review = "";
        }
        if (newError.score && newError.review) {
            newError.review = "";
        }
        if (newError.name && newError.review) {
            newError.review = "";
        }
        if (input.name && input.score > 0 && input.score < 5 && input.review) {
            input.name = "";
            input.score = "";
            input.review = "";
            alert("Форма отправлена");
        }
        
        setError(newError);
    };

    return (
        <div className='form'>
            <form className="user-form"
                noValidate
                onSubmit={handleSubmit}>
                <fieldset className="user-field">
                    <legend className="user-legend">
                        Добавить свой отзыв
                    </legend>

                    <div className="box-forma">
                        <div className="bloc-forma">
                            <div className="box-foma-users">

                                <input className={`user-name ${error?.name ? 'error' : ""}`}
                                    type="text"
                                    id="name"
                                    name="name"
                                    placeholder="Имя и Фамилия"
                                    value={input?.name}
                                    onChange={handleChangeName}
                                    onFocus={handleFocusName} />
                                <div className={`error-user-name ${error?.name ? '' : 'hidden'}`}>{error?.name}</div>
                            </div>

                            <div className="box-foma-score">
                                <input className={`user-score ${error?.score ? 'error' : ""}`}
                                    name="score"
                                    type="number"
                                    value={input?.score}
                                    placeholder="Оценка"
                                    onChange={handleChangeScore}
                                    onFocus={handleFocusScore} />
                                <div className={`error-user-score ${error?.score ? '' : 'hidden'}`}>{error?.score}</div>
                            </div>
                        </div>
                    </div>
                    <div className='blok-review'>
                        <textarea className={`user-review ${error?.review ? 'error' : ""}`}
                            id="review"
                            name="review"
                            placeholder="Текст отзыва"
                            onChange={handleChangeReview}
                            onFocus={handleFocusReview}
                            value={input?.review} />
                        <div className={`error-review ${error?.review ? "" : 'hidden'}`}>{error?.review}</div>
                    </div>
                    <button className="user-review__btn" type="submit">Отправить отзыв</button>
                </fieldset>
            </form>
        </div>
    );
}
export default Forma;