"use strict";

class myForm {
    finish() {
        alert("Форма отправлена");
    }
}

class Form extends myForm {
    constructor(
        userForm,
        userName,
        userScore,
        userReview,
        errorName,
        errorScore,
        errorReview
    ) {
        super()
        this.form = document.querySelector(userForm);//форма

        this.name = document.querySelector(userName); //поле ввода ФИО
        this.score = document.querySelector(userScore); //поле ввода Оценки
        this.review = document.querySelector(userReview); //поле ввода Отзыва

        this.errorName = document.querySelector(errorName); //div ошибка ФИО
        this.errorScore = document.querySelector(errorScore); //div ошибка Оценки
        this.errorReview = document.querySelector(errorReview); // div ошибка Отзыва

        this.name.value = localStorage.getItem('name');
        this.score.value = localStorage.getItem('score');
        this.review.value = localStorage.getItem('review');

        this.form.addEventListener("submit", this.validate.bind(this));
        this.name.addEventListener("focus", this.clearErrorName.bind(this));
        this.score.addEventListener("focus", this.clearErrorScore.bind(this));
        this.review.addEventListener("focus", this.clearErrorReview.bind(this));

        this.name.addEventListener("input", this.nameInput.bind(this));
        this.score.addEventListener("input", this.scoreInput.bind(this));
        this.review.addEventListener("input", this.reviewInput.bind(this));
    }

    validate(event) {
        event.preventDefault();

        let nameLength = this.name.value.trim().length;
        let score = +this.score.value.trim();
        let reviewLength = this.review.value.trim().length;
        if (nameLength === 0) {
            this.fullErrorName();
            return;
        } else if (nameLength < 2) {
            this.fullErrorName2();
            return;
        } else if (score < 1 || score > 5) {
            this.fullErrorScore();
            return;
        } else if (reviewLength === 0) {
            this.fullErrorReview();
            return;
        } else if (reviewLength < 5) {
            this.fullErrorReview2();
            return;
        }

        this.form.reset();
        localStorage.removeItem("name");
        localStorage.removeItem("score");
        localStorage.removeItem("review");
        this.finish();
        console.log("ok")
    }
    fullErrorName() {
        this.errorName.innerHTML = "Вы забыли указать имя и фамилию";
        this.errorName.style.visibility = "visible";
    }
    fullErrorName2() {
        this.errorName.innerHTML = "Имя не может быть короче 2-х символов";
        this.errorName.style.visibility = "visible";
    }
    fullErrorScore() {
        this.errorScore.innerHTML = "Укажите число от 1 до 5";
        this.errorScore.style.visibility = "visible";
    }
    fullErrorReview() {
        this.errorReview.innerHTML = "Вы забыли оставить отзыв";
        this.errorReview.style.visibility = "visible";
    }
    fullErrorReview2() {
        this.errorReview.innerHTML = "Отзыв не должен быть короче 5-и символов";
        this.errorReview.style.visibility = "visible";
    }
    nameInput() {
        localStorage.setItem("name", this.name.value);
    }
    scoreInput() {
        localStorage.setItem("score", this.score.value);
    }
    reviewInput() {
        localStorage.setItem("review", this.review.value);
    }
    clearErrorName() {
        this.errorName.style.visibility = "hidden";
    }
    clearErrorScore() {
        this.errorScore.style.visibility = "hidden";
    }
    clearErrorReview() {
        this.errorReview.style.visibility = "hidden";
    }

}
let review = new Form(
    ".user-form",
    ".user-name",
    ".user-score",
    ".user-review",
    ".error-user-name",
    ".error-user-score",
    ".error-review"
);