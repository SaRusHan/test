"use strict";  //строгий режим


// упражнение №1

let arr1 = [1, 2, 10, 5];

function getSumm(arr) {
    return arr.reduce((acc, curr) => acc + curr, 0);
}

console.log(getSumm(arr1));


let arr2 = ["a", {}, 3, 3, -2];

function getSumm(arr2) {
    let sum = 0;
    for (let n = 0; n < arr2.length; n++) {
        if (isNaN(arr2[n])) continue;
        sum += arr2[n];
    }

    return sum;
}

console.log(getSumm(arr2));


// упражнение №3

let cart = [4884];
function addToCart(produktId) {
    let hasInCard = cart.includes(produktId);
    if (hasInCard) return;
    cart.push(produktId);
}
addToCart(3456);

console.log(cart);

function removeFromCart(produktId) {

    cart = cart.filter(function (Id) {
        return Id != (produktId);
    });
}
removeFromCart(4884);
console.log(cart);
