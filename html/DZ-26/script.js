"use strict";  

let choiceSelector = document.querySelector(".logo__choice");
let basketSelector = document.querySelector(".sale__basket");
let goodChoice = localStorage.getItem("goodChoice");
render();

document.querySelector(".sale__basket").addEventListener("click", changeBasket);

function changeBasket() {
    goodChoice = 1 - goodChoice;
    localStorage.setItem("goodChoice", goodChoice);
    render();
}

function render() {
    if (goodChoice) {
        choiceSelector.innerHTML = goodChoice
        choiceSelector.style.visibility = "visible";
        basketSelector.style.background = "#888888";
        basketSelector.classList.remove("zero");
    } else {
        choiceSelector.style.visibility = "hidden";
        basketSelector.classList.add("zero");
        basketSelector.style.background = "#F36223";
        localStorage.removeItem("goodChoice");
    }
}

let favoritSelector = document.querySelector(".favourites__choice");
let laikSelector = document.querySelector(".sale__tender__favourites");
let laikChoice = localStorage.getItem("laikChoice");
render2();

document.querySelector(".sale__tender__favourites").addEventListener("click", changeLaik);

function changeLaik() {
    laikChoice = 1 - laikChoice;
    localStorage.setItem("laikChoice", laikChoice);
    render2();
}
function render2() {
    if (laikChoice) {
        favoritSelector.innerHTML = laikChoice;
        favoritSelector.style.visibility = "visible";
        laikSelector.classList.add("coise");
    } else {
        favoritSelector.style.visibility = "hidden";
        laikSelector.classList.remove("coise");
        localStorage.removeItem("laikChoice");

    }
}

