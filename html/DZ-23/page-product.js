"use strict";

let form = document.querySelector(".user-form");//форма

let inputName = form.querySelector(".user-name"); //ввод данных
let errorNameElem = form.querySelector(".error-users-name"); //div ошибка

let inputScore = form.querySelector(".user-score");//ввод данных
let errorScoreElem = form.querySelector(".error-users-score"); //div ошибка

let errorName = '';
form.addEventListener("submit", validate);
inputName.addEventListener("focus", clearErrorName);
inputScore.addEventListener("focus", clearScoreName);

function validate(event) {
    event.preventDefault();
    // console.log("validatet");


    let name = inputName.value.trim().length;
    let errorScore = '';
    let score = +inputScore.value;
    if (name === 0) {
        errorName = "Вы забыли указать имя и фамилию";
        errorNameElem.innerHTML = errorName;
        errorNameElem.style.visibility = "visible";
        return;
    }
    if (name < 2) {
        errorName = "Имя не может быть короче 2-х символов";
        errorNameElem.innerHTML = errorName;
        errorNameElem.style.visibility = "visible";
        return;
    }
    if (score < 1 || score > 5) {
        errorScore = 'Укажите число от 1 до 5';
        errorScoreElem.innerHTML = errorScore;
        errorScoreElem.style.visibility = "visible";
        return;
    }
    console.log("ok")
}

function clearErrorName() {
    errorNameElem.style.visibility = "hidden";
}
function clearScoreName() {
    errorScoreElem.style.visibility = "hidden";

}

