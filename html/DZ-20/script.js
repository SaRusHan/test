"use strict";  //строгий режим

// упражнение №1

/**
 * Коментарий, что делает функция.
*@param {Object}obj Проверяем объект.
*@return {boolean} Возвращает свойства функции.
 * 
 *  */

function isEmpty(obj) {
    for (let key in obj) {
        // если тело цикла начнет выполняться - значит в объекте есть свойства
        return false;
    }
    return true;
}

// упражнение №3

/**Коментарий функции, сложение всех увеличенных з/п
 * @param {Object}salaries Проверяемый список.
 * @param {Object}newSalaries Суммы увеличенные з/п на процент.
 * @return {number} Возвращает сумму умноженное на процент.
 * @param {Object}summ  Сумма всех з/п после увеличения.
 */
let salaries = {
    John: 100000,  //100000 5 000
    Ann: 160000,   //160000 8 000
    Pete: 130000,  //130000  6 500          409 500
};

function raiseSalary(perzent) {
    let newSalaries = {};
    for (let key in salaries) {
    let raise = (salaries[key] * perzent) / 100;
    newSalaries[key] = salaries[key] + raise;
    }
    return newSalaries;
}


function calcSumm(obj) {
    let summ = 0;
    for (let key in obj) {
        summ = summ + obj[key];
     
    }

    return summ;

}
let newSalaries = raiseSalary(5);
let beforeSumm = calcSumm(salaries);
let summ = calcSumm(newSalaries);
console.log(summ);




