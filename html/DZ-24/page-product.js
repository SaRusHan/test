"use strict";

let form = document.querySelector(".user-form");//форма

let inputName = form.querySelector(".user-name"); //поле ввода ФИО
let inputScore = form.querySelector(".user-score"); //поле ввода Оценки
let inputReview = form.querySelector(".user-review"); //поле ввода Отзыва

let errorNameElem = form.querySelector(".error-users-name"); //div ошибка ФИО
let errorScoreElem = form.querySelector(".error-users-score"); //div ошибка Оценки
let errorReviewElem = form.querySelector(".error-review"); // div ошибка Отзыва


form.addEventListener("submit", validate);
inputName.addEventListener("focus", clearErrorName);
inputScore.addEventListener("focus", clearScoreName);
inputReview.addEventListener("focus", clearReviewName);

inputName.addEventListener("input", nameInput);
inputScore.addEventListener("input", scoreInput);
inputReview.addEventListener("input", reviewInput);

function nameInput() {
    localStorage.setItem("name", inputName.value);
}
function scoreInput() {
    localStorage.setItem("score", inputScore.value);
}
function reviewInput() {
    localStorage.setItem("review", inputReview.value);
}


inputName.value = localStorage.getItem('name');
inputScore.value = localStorage.getItem('score');
inputReview.value = localStorage.getItem('review');



function validate(event) {
    event.preventDefault();

    let name = inputName.value.trim().length;
    let errorName = "";
    let score = +inputScore.value;
    let errorScore = "";
    let review = inputReview.value.trim().length;
    let errorReview = "";
    if (name === 0) {
        errorName = "Вы забыли указать имя и фамилию";
        errorNameElem.innerHTML = errorName;
        errorNameElem.style.visibility = "visible";
        return;
    } if (name < 2) {
        errorName = "Имя не может быть короче 2-х символов";
        errorNameElem.innerHTML = errorName;
        errorNameElem.style.visibility = "visible";
        return;
    } if (score < 1 || score > 5) {
        errorScore = 'Укажите число от 1 до 5';
        errorScoreElem.innerHTML = errorScore;
        errorScoreElem.style.visibility = "visible";
        return;
    } if (review === 0) {
        errorReview = "Вы забыли оставить отзыв";
        errorReviewElem.innerHTML = errorReview;
        errorReviewElem.style.visibility = "visible";
        return;
    } if (review < 5) {
        errorReview = "Отзыв не должен быть короче 5-и символов";
        errorReviewElem.innerHTML = errorReview;
        errorReviewElem.style.visibility = "visible";
        return;
    }
    form.reset();
    localStorage.clear();
    console.log("ok")
}

function clearErrorName() {
    errorNameElem.style.visibility = "hidden";
}
function clearScoreName() {
    errorScoreElem.style.visibility = "hidden";
}
function clearReviewName() {
    errorReviewElem.style.visibility = "hidden";
}


